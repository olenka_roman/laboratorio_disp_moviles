package com.example.broman.colectdata;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements OnItemSelectedListener {



    private final String[] datos = {"ListView", "GridView","Spinner","RadioButton"};
    private ArrayAdapter<String> adapter;
    private ListView lvOpt;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);


        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,datos);

        lvOpt = (ListView) findViewById(R.id.lvSemana);


        lvOpt.setAdapter(adapter);

        lvOpt.setOnItemClickListener(

                new AdapterView.OnItemClickListener(){


                    public void onItemClick(AdapterView<?> parent, View v, int pos, long id){
                        if(pos==0){
                            Intent intent = new Intent(getApplicationContext(),Main3Activity.class);
                            startActivity(intent);
                        }
                        if(pos==1){
                            Intent intent = new Intent(getApplicationContext(),Main2Activity.class);
                            startActivity(intent);
                        }
                        if(pos==2){
                            Intent intent = new Intent(getApplicationContext(),Main4Activity.class);
                            startActivity(intent);
                        }
                        if(pos==3){
                            Intent intent = new Intent(getApplicationContext(),group.class);
                            startActivity(intent);
                        }

                    }
                }

        );


    }

    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

        Toast.makeText(getApplicationContext(), (String)parent.getItemAtPosition(pos), Toast.LENGTH_SHORT).show();


    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }




}
