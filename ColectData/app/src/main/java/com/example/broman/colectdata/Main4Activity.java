package com.example.broman.colectdata;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

public class Main4Activity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Spinner cmbOpt;
    private ArrayAdapter<CharSequence> adapter;
    private ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        cmbOpt = (Spinner) findViewById(R.id.spinOpt);
        adapter = ArrayAdapter.createFromResource(this,R.array.autos, android.R.layout.simple_list_item_1);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        img = (ImageView) findViewById(R.id.ivAutos);
        cmbOpt.setOnItemSelectedListener(this);
        cmbOpt.setAdapter(adapter);


    }

    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

        Toast.makeText(getApplicationContext(), (String)parent.getItemAtPosition(pos), Toast.LENGTH_SHORT).show();
        if(pos==0)
        {
            img.setImageResource(R.drawable.bmw_serie6_cabrio_2015);
        }
        if(pos==1)
        {
            img.setImageResource(R.drawable.jaguar_xe);
        }
        if(pos==2)
        {
            img.setImageResource(R.drawable.ford_mondeo);
        }
        if(pos==3)
        {
            img.setImageResource(R.drawable.porsche_911_gts);
        }if(pos==4)
        {
            img.setImageResource(R.drawable.volkswagen_golf_r_variant_2015);
        }


    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }
}
