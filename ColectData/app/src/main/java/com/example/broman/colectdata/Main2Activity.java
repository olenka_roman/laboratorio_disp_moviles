package com.example.broman.colectdata;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    private Integer[] imagenesID={  R.drawable.bmw_serie6_cabrio_2015,R.drawable.ford_mondeo,
            R.drawable.jaguar_f_type_2015, R.drawable.mazda_mx5_2015,
            R.drawable.mercedes_benz_amg_gt,R.drawable.jaguar_xe,
            R.drawable.porsche_911_gts,R.drawable.seat_leon_st_cupra
    };
    GridView grid_autos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        grid_autos = (GridView) findViewById(R.id.gvAutos);
        grid_autos.setAdapter(new adaptarImagenGrid(this));

        grid_autos.setOnItemClickListener(

                new AdapterView.OnItemClickListener(){

                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    }


                    public void onNothingSelected(AdapterView<?> parent) {

                    }

                    public void onItemClick(AdapterView<?> parent, View v, int pos, long id){
                        Toast.makeText(getBaseContext(),"Item: "+(pos+1)+" Seleccionado", Toast.LENGTH_SHORT).show();
                    }
                }

        );
    }

    public class adaptarImagenGrid extends BaseAdapter {
        private Context imagenContext;
        public adaptarImagenGrid(Context context){
            imagenContext= context;
        }


        public int getCount() {
            return imagenesID.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imagen;

            if(convertView==null){
                imagen = new ImageView(imagenContext);
                imagen.setLayoutParams(new GridView.LayoutParams(200,200));
                imagen.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imagen.setPadding(16,16,16,16);

            }else{
                imagen = (ImageView)convertView;
            }
            imagen.setImageResource(imagenesID[position]);

            return imagen;
        }


    }
}
