package com.example.broman.colectdata;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

public class Main3Activity extends AppCompatActivity {

    private ArrayAdapter<CharSequence> adapter;
    private ListView lvOpt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        adapter = ArrayAdapter.createFromResource(this,R.array.valores_array, android.R.layout.simple_list_item_1);

        lvOpt = (ListView) findViewById(R.id.lvContactos);
        lvOpt.setAdapter(adapter);

    }
}
