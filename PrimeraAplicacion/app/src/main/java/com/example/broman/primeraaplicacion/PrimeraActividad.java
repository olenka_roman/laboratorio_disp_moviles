package com.example.broman.primeraaplicacion;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class PrimeraActividad extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_primera_actividad);
    }

    public void pulsado(View view){
        Intent intent = new Intent(this,SegundaActividad.class);
        startActivity(intent);
        finish();
    }
}
