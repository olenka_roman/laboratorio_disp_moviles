package com.example.broman.activitiesintent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private Button btnLogin;
    private EditText etName,etPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        etName = (EditText) findViewById(R.id.etName);
        etPass = (EditText) findViewById(R.id.etPass);
    }

//    El evento para enviar los datos

    public void loguear(View view){
        Intent intent = new Intent(this,segundaActividad.class);

        String nombre=etName.getText().toString();
        String password=etPass.getText().toString();

        intent.putExtra("nombre",nombre);
        intent.putExtra("password",password);


        startActivity(intent);

    }
}
