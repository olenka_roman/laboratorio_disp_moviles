package com.example.broman.activitiesintent;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.RequiresPermission;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class segundaActividad extends AppCompatActivity {

    private TextView tvNom, tvNum;
    private String numero;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda_actividad);
        Intent intent = getIntent();

        tvNom = (TextView) findViewById(R.id.tv_Nom);
        tvNum = (TextView) findViewById(R.id.tv_Num);
// Se reciben los datos enviados de la primera actividad
        Bundle bund = intent.getExtras();
        tvNom.setText(bund.getString("nombre"));
        numero = bund.getString("password");
        tvNum.setText(numero);


    }



    public void llamar(@RequiresPermission View presionar) {
        Intent llamando = new Intent(Intent.ACTION_CALL);

//        Se setea los datos obtenidos y luego se procede a llamar cuando se presiona sobre el texto
        String telf = "tel:"+numero;


        llamando.setData(Uri.parse(telf));


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        startActivity(llamando);
    }
}
