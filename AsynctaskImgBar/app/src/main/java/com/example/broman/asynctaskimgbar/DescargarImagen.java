package com.example.broman.asynctaskimgbar;

import android.app.ProgressDialog;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;

public class DescargarImagen extends AppCompatActivity {

    private String getURL;
    private ImageView img;
    //private File path;
    TaskStackBuilder stackBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descargar_imagen);

        Intent intent = getIntent();

        Bundle obtenerData = intent.getExtras();

        img = (ImageView) findViewById(R.id.ivImagen);
        getURL = obtenerData.getString("url");

        Descargar imagen = new Descargar();
        imagen.execute(getURL);

    }

    private class Descargar extends AsyncTask<String,String,Bitmap>{

        ProgressDialog barra;
        Bitmap imgObj;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            barra = new ProgressDialog(DescargarImagen.this);
            barra.setMessage("Descargando Imagen");
            barra.setCancelable(false);
            barra.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            barra.setProgress(0);
            barra.setIndeterminate(false);
            barra.show();

        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Log.i("doInBackground" , "Entra en doInBackground");

            URL imageUrl = null;
            Bitmap imagen = null;
            try{
                imageUrl = new URL(params[0]);
                HttpURLConnection conexion = (HttpURLConnection) imageUrl.openConnection();
                conexion.connect();
                imagen = BitmapFactory.decodeStream(conexion.getInputStream());

                int tamImagen = conexion.getContentLength();

                InputStream entrada = new BufferedInputStream(imageUrl.openStream(),10000);
                OutputStream salida = new FileOutputStream(Environment.getExternalStorageDirectory().getPath()+"/descarga.jpg");

                byte data[] = new byte[1024];
                long total =0;
                int  cont= entrada.read(data);

                while(cont!=-1){
                    total += cont;
                    publishProgress(String.valueOf((int)((total*100)/tamImagen)));
                    salida.write(data, 0, cont);
                    cont = entrada.read(data);
                }
                salida.flush();
                salida.close();
                entrada.close();

            }catch(IOException ex){
                ex.printStackTrace();
            }

            imgObj=imagen;
            //

            return imagen;
        }

        protected void onProgressUpdate(String... progress) {
            Log.d("ANDRO_ASYNC",progress[0]);
            barra.setProgress(Integer.parseInt(progress[0]));
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        protected void onPostExecute(Bitmap imagen) {
            super.onPostExecute(imagen);
            img.setImageBitmap(imagen);
            barra.dismiss();
            NotificationCompat.Builder mBuilder;
            NotificationManager mNotifyMgr =(NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);

            int icono = R.mipmap.ic_launcher;

            PendingIntent pendingIntent;
            mBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(getApplicationContext())
                    .setSmallIcon(icono)
                    .setContentTitle("Imagen Descargada")
                    .setContentText("La imagen se descargo")
                    .setTicker("Alerta")
                    .setAutoCancel(true);

            stackBuilder = TaskStackBuilder.create(DescargarImagen.this);
            Intent i=new Intent(/*DescargarImagen.this, DescargarImagen.class*/);
            i.setAction(Intent.ACTION_VIEW);
            i.setDataAndType(Uri.parse("file://" +Environment.getExternalStorageDirectory().toString()+ "/descarga.jpg"), "image/*");


            stackBuilder.addNextIntent(i);
            pendingIntent =  stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(pendingIntent);

            mNotifyMgr.notify(1, mBuilder.build());
        }
    }

    /*private String getCurrentDateAndTime() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd-HH-mm-­ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }*/
}