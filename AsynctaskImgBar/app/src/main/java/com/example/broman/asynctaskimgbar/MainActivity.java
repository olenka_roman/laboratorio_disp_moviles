package com.example.broman.asynctaskimgbar;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private Button btnD;
    private EditText getURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnD = (Button) findViewById(R.id.btnDescarga);
        getURL = (EditText) findViewById(R.id.etURL);

    }

    public void Descarga(View view){
//En nuestra actividad principal lo que hacemos es enviar nuestro URL y luego presionamos un boton  que envia el String con la url
        Intent intent = new Intent(this,DescargarImagen.class);
        String url = getURL.getText().toString();

        intent.putExtra("url",url);

        startActivity(intent);


    }

}
